package com.leyou.web;


import com.leyou.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
public class UploadController {

    @Autowired
    private UploadService uploadService;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @PostMapping("image")
    public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile file) {

        String url = uploadService.uploadImage(file);

        return ResponseEntity.status(HttpStatus.OK).body(url);
    }

    /**
     * 获取图片上传签名
     * @return
     */
    @GetMapping("signature")
    public ResponseEntity<Map<String, Object>> getSignature() {

        return ResponseEntity.ok(uploadService.getSignature());
    }
}
