package com.leyou.item.mapper;

import com.leyou.item.entity.Category;
import tk.mybatis.mapper.common.Mapper;

public interface CategoryMapper extends Mapper<Category> {
}
