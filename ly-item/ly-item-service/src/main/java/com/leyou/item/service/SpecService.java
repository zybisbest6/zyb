package com.leyou.item.service;

import com.leyou.common.exception.ExceptionEnum;
import com.leyou.common.exception.LyException;
import com.leyou.common.utils.BeanHelper;
import com.leyou.item.dto.SpecGroupDTO;
import com.leyou.item.dto.SpecParamDTO;
import com.leyou.item.entity.SpecGroup;
import com.leyou.item.entity.SpecParam;
import com.leyou.item.mapper.SpecGroupMapper;
import com.leyou.item.mapper.SpecParamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@Service
public class SpecService {

    @Autowired
    private SpecGroupMapper specGroupMapper;

    @Autowired
    private SpecParamMapper specParamMapper;


    //根据种类id查询规格组
    public List<SpecGroupDTO> queryGroupByCategory(Long cid) {

        SpecGroup specGroup = new SpecGroup();
        specGroup.setCid(cid);

        List<SpecGroup> list = specGroupMapper.select(specGroup);
        if (CollectionUtils.isEmpty(list)) {
            throw new LyException(ExceptionEnum.SPEC_NOT_FOUND);
        }

        List<SpecGroupDTO> specGroupDTOS = BeanHelper.copyWithCollection(list, SpecGroupDTO.class);

        return specGroupDTOS;
    }

    //根据规格组id查询规格参数
    public List<SpecParamDTO> querySpecParams(Long gid) {

        SpecParam specParam = new SpecParam();
        specParam.setGroupId(gid);

        List<SpecParam> list = specParamMapper.select(specParam);
        if (CollectionUtils.isEmpty(list)) {
            throw new LyException(ExceptionEnum.SPEC_NOT_FOUND);
        }

        List<SpecParamDTO> paramsDTO = BeanHelper.copyWithCollection(list, SpecParamDTO.class);

        return paramsDTO;
    }


    //新增规格组
    @Transactional
    public void addSpecGroup(SpecGroupDTO specGroupDTO) {
        SpecGroup specGroup = BeanHelper.copyProperties(specGroupDTO, SpecGroup.class);

        int count = specGroupMapper.insertSelective(specGroup);
        if (count != 1) {
            throw new LyException(ExceptionEnum.INSERT_OPERATION_FAIL);
        }

    }

    //删除规格组
    @Transactional
    public void deleteSpecGroup(Long gid) {
        SpecGroup specGroup = new SpecGroup();
        specGroup.setId(gid);


        int count = specGroupMapper.delete(specGroup);
        if (count != 1) {
            throw new LyException(ExceptionEnum.DELETE_OPERATION_FAIL);
        }


    }

    //修改规格组
    @Transactional
    public void updateSpecGroup(SpecGroupDTO specGroupDTO) {
        SpecGroup specGroup = BeanHelper.copyProperties(specGroupDTO, SpecGroup.class);
        specGroup.setUpdateTime(new Date());

        int count = specGroupMapper.updateByPrimaryKeySelective(specGroup);
        if (count != 1) {
            throw new LyException(ExceptionEnum.UPDATE_OPERATION_FAIL);
        }


    }

    //新增规格参数
    public void addSpecParam(SpecParamDTO specParamDTO) {

        SpecParam specParam = BeanHelper.copyProperties(specParamDTO, SpecParam.class);
        int count = specParamMapper.insertSelective(specParam);
        if (count != 1) {
            throw new LyException(ExceptionEnum.INSERT_OPERATION_FAIL);
        }
    }

    //修改规格参数
    public void updateSpecParam(SpecParamDTO specParamDTO) {

        SpecParam specParam = BeanHelper.copyProperties(specParamDTO, SpecParam.class);
        specParam.setUpdateTime(new Date());

       // int count = specParamMapper.insertSelective(specParam);
        int count = specParamMapper.updateByPrimaryKeySelective(specParam);
        if (count != 1) {
            throw new LyException(ExceptionEnum.UPDATE_OPERATION_FAIL);
        }

    }

    //删除规格参数
    public void deleteSpecParam(Long id) {

        SpecParam specParam = new SpecParam();
        specParam.setId(id);

        int count = specParamMapper.deleteByPrimaryKey(specParam);
        if (count != 1) {
            throw new LyException(ExceptionEnum.DELETE_OPERATION_FAIL);
        }

    }
}
