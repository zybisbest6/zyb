package com.leyou.item.service;

import com.leyou.common.utils.BeanHelper;
import com.leyou.item.dto.CategoryDTO;
import com.leyou.item.entity.Category;
import com.leyou.item.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import java.util.List;


@Service
public class CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    public List<CategoryDTO> queryByParentId(Long pid) {

        Category category = new Category();
        category.setParentId(pid);

        List<Category> categoryList = categoryMapper.select(category);

        if (CollectionUtils.isEmpty(categoryList)) {
            //TODO 抛出异常

        }

        List<CategoryDTO> categoryDTOList = BeanHelper.copyWithCollection(categoryList, CategoryDTO.class);

        return categoryDTOList;
    }
}
