package com.leyou.item.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.leyou.common.exception.ExceptionEnum;
import com.leyou.common.exception.LyException;
import com.leyou.common.utils.BeanHelper;
import com.leyou.common.vo.PageResult;
import com.leyou.item.dto.BrandDTO;
import com.leyou.item.entity.Brand;
import com.leyou.item.mapper.BrandMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class BrandService {


    @Autowired
    private BrandMapper brandMapper ;

    //品牌的分页查询
    public PageResult<BrandDTO> queryBrandByPage(
            Integer page, Integer rows, String key, String sortBy, Boolean desc) {

        //分页
        PageHelper.startPage(page, rows);
        //过滤条件
        Example example = new Example(Brand.class);
        if (StringUtils.isNoneBlank(key)) {
            example.createCriteria().orLike("name", "%" + key + "%").
                    orEqualTo("letter", key.toUpperCase());

        }
        //排序
        if(StringUtils.isNoneBlank(sortBy)) {
            String orderByClause = sortBy + (desc ? " DESC" : " ASC");
            example.setOrderByClause(orderByClause);// id desc
        }

        //查询
        List<Brand> brands = brandMapper.selectByExample(example);

        // 判断是否为空
        if(CollectionUtils.isEmpty(brands)){
            //TODO 抛出异常
            throw new LyException(ExceptionEnum.BRAND_NOT_FOUND);
        }

        //解析分页结果
        PageInfo<Brand> info = new PageInfo<>(brands);

        //数据转移
        List<BrandDTO> list = BeanHelper.copyWithCollection(brands, BrandDTO.class);

        PageResult pageResult = new PageResult<BrandDTO>(info.getTotal(),list);

        return pageResult;
    }




    //品牌新增
    @Transactional
    public void saveBrand(BrandDTO brandDTO, List<Long> cids) {
        //属性拷贝, 因为通用mapper的插入insertSelective 里面传brand
        Brand brand = BeanHelper.copyProperties(brandDTO, Brand.class);

        //insertSelective 为null的字段忽略，而insert 会把为null的字段也插入
        int count = brandMapper.insertSelective(brand);
        if (count == 1) {
            throw new LyException(ExceptionEnum.INSERT_OPERATION_FAIL);
        }

        count = brandMapper.insertCategoryBrand(brand.getId(), cids);
        if (count == 1) {
            throw new LyException(ExceptionEnum.INSERT_OPERATION_FAIL);
        }
    }

    /**
     * 删除品牌
     * @param id
     */
    @Transactional
    public void deleteBrand(Long id) {
        Brand brand = new Brand();
        brand.setId(id);

        //删除品牌表的里的  品牌
        int result = brandMapper.deleteByPrimaryKey(brand);
        if (result != 1) {
            throw new LyException(ExceptionEnum.DELETE_OPERATION_FAIL);
        }

        // 品牌和种类 是多对多 有中间表，所以删除中间中品牌的数据
        result = brandMapper.deleteCategoryBrand(id);

        if (result < 0) {
            throw new LyException(ExceptionEnum.DELETE_OPERATION_FAIL);
        }

    }
}
