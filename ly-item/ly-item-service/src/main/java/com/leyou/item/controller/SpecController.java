package com.leyou.item.controller;


import com.leyou.item.dto.SpecGroupDTO;
import com.leyou.item.dto.SpecParamDTO;
import com.leyou.item.service.SpecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SpecController {

    @Autowired
    private SpecService specService;

    /**
     * 根据分类id 查询 规格组
     *
     * @param cid
     * @return
     */
    @GetMapping("spec/groups/of/category")
    public ResponseEntity<List<SpecGroupDTO>> queryGroupByCategory(@RequestParam("id") Long cid) {

        List<SpecGroupDTO> list = specService.queryGroupByCategory(cid);

        return ResponseEntity.ok(list);

    }


    /**
     * 根据规格组id 查询 规格参数
     *
     * @param gid
     * @return
     */
    @GetMapping("spec/params")
    public ResponseEntity<List<SpecParamDTO>> querySpecParams(@RequestParam("gid") Long gid) {


        List<SpecParamDTO> list = specService.querySpecParams(gid);

        return ResponseEntity.ok(list);
    }


    /**
     * 新增规格组
     *
     * @param specGroupDTO
     * @return
     */

    @PostMapping(name = "spec/group")
    public ResponseEntity<Void> addSpecGroup(@RequestBody SpecGroupDTO specGroupDTO) {

        specService.addSpecGroup(specGroupDTO);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


    /**
     * 删除规格组
     *
     * @param gid
     * @return
     */
    @DeleteMapping(value = "spec/group/{gid}")
    public ResponseEntity<Void> deleteSpecGroup(@PathVariable("gid") Long gid) {

        specService.deleteSpecGroup(gid);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    /**
     * 修改规格组
     *
     * @param specGroupDTO
     * @return
     */
    @PutMapping("spec/group")
    public ResponseEntity<Void> updateSpecGroup(@RequestBody SpecGroupDTO specGroupDTO) {

        specService.updateSpecGroup(specGroupDTO);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    /**
     * 新增规格参数
     *
     * @param specParamDTO
     * @return
     */
    @PostMapping("spec/param")
    public ResponseEntity<Void> addSpecParam(@RequestBody SpecParamDTO specParamDTO) {

        specService.addSpecParam(specParamDTO);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 修改规格参数
     *
     * @param specParamDTO
     * @return
     */
    @PutMapping("spec/param")
    public ResponseEntity<Void> updateSpecParam(@RequestBody SpecParamDTO specParamDTO) {

        specService.updateSpecParam(specParamDTO);
        return ResponseEntity.status(203).build();

    }

    /**
     * 删除规格参数
     * @param id
     * @return
     */
    @DeleteMapping(value = "spec/param/{id}")
    public ResponseEntity<Void> deleteSpecParam(@PathVariable("id") Long id) {

        specService.deleteSpecParam(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
