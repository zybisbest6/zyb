package com.leyou.item.mapper;

import com.leyou.item.entity.Brand;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface BrandMapper extends Mapper<Brand> {

    /**
     * 新增商品分类 和品牌中间表数据
     * @param bid
     * @param ids
     * @return
     */
    int insertCategoryBrand(@Param("bid") Long bid,@Param("ids") List<Long> ids);

    /**
     * 删除种类品牌中间表
     * @param bid
     * @return
     */
    int deleteCategoryBrand(@Param("bid") Long bid);
}
