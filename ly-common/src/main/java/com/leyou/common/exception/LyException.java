package com.leyou.common.exception;



import lombok.Data;


/**
 * 自定义异常类
 */
@Data
public class LyException extends RuntimeException {
    private int status;

    public LyException(ExceptionEnum em) {
        super(em.getMessage());

        this.status = em.getStatus();
    }


    public LyException(ExceptionEnum em,Throwable cause) {
        super(em.getMessage(),cause);
        this.status = em.getStatus();
    }
}
