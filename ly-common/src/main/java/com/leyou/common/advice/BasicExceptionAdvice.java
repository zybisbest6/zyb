package com.leyou.common.advice;


import com.leyou.common.exception.LyException;
import com.leyou.common.vo.ExceptionResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * 拦截controller层 异常信息,并且帮助controller层返回异常结果
 */
@ControllerAdvice //默认拦截所有加了@Controller的类, 也就是默认扫描controller层下的类
public class BasicExceptionAdvice {

    @ExceptionHandler(LyException.class) //拦截和监听这个类型异常 并把抛出的异常传递过来
    public ResponseEntity<ExceptionResult> handlerException(LyException e) {
            //并且帮controller返回异常结果
        return ResponseEntity.status(e.getStatus()).body(new ExceptionResult(e));
    }

}
